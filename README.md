# Prometheus File Client

## How it works

### Flow diagram

![Flow Diagram](./diagram.png "Flow Diagram")

---

We write the prometheus metrics periodically into a specific folder(/data/metrics/<pod_name><pid>.prom, in every project. This file is read by a node_exporter pod, which sits on every kubernetes node. Prometheus server, scrapes the node_exporter pod, for metrics.

## How to install

```
npm i prom-file-client
```

Example avaialble at below link:

https://gitlab.com/fynd/prom-file-client/blob/master/examples/metrics-example.js

You need to use `/data/metrics/` for saving metrics file as shown in the example

## Conventions

### **_Must have labels_**

- pod_name
- pod_namespace

Based on these labels, you can get the data using promQL, You can add more labels, depending on your query requirements
