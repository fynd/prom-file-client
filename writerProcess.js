const fs = require("fs").promises;
const Queue = require("queue-fifo");
const queue = new Queue();
let PROCESSING = false;
let SIGTERM = false;

process.on("SIGTERM", () => {
  SIGTERM = true;
  console.log("SIGTERM sig received by child");
  process.exit(0);
});

process.on("message", (msg) => {
  queue.enqueue(msg);
});

async function writeMetrics(msg) {
  try {
    if (SIGTERM) {
      console.log("Skipping metrics write as SIGTERM received");
      return;
    }
    await fs.writeFile(msg.file, msg.data, { flags: "wx" });
  } catch (error) {
    console.log({ status: "failed", message: "Write failed for " + msg.file });
  }
}

setInterval(async () => {
  if (PROCESSING) {
    return;
  }
  PROCESSING = true;
  while (!queue.isEmpty()) {
    await writeMetrics(queue.dequeue());
  }
  PROCESSING = false;
}, 1000);
