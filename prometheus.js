const Counter = require("./metrics/counter");
const Gauge = require("./metrics/gauge");
const Histogram = require("./metrics/histogram");
const Summary = require("./metrics/summary");

module.exports = {
    Counter: Counter,
    Gauge: Gauge,
    Histogram: Histogram,
    Summary: Summary
}
