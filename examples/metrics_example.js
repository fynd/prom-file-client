"use strict";

const client = require("../prometheus")
let _counter = 0;

const opts = {
    metricsDir: __dirname,
    flushIntervalms: 4000
};

let register = require('../fileRegistry').getInstance(opts);

const counter = new client.Counter({
    name: 'xxxxx_metric_name',
    help: 'xxxxx_metric_help',
    labelNames: ["pod_name"],
    register: register
});

const gauge = new client.Gauge({
    name: 'Guage_metric_name',
    help: 'Guage_metric_help',
    labelNames: ["pod_name", "method", "statusCode"],
    register: register
});

const gauge1 = new client.Gauge({
    name: 'Guage_inc_demo',
    help: 'Guage_inc_demo_help',
    labelNames: ["pod_name"],
    register: register
});

const histogram = new client.Histogram({
    name: 'counter_increment_ms',
    help: 'Duration of counter Increment in ms',
    register: register,
    labelNames: ['pod_name'],
    // buckets for response time from 0.1ms to 500ms
    buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]
});

const histogram1 = new client.Histogram({
    name: 'timer_increment_ms',
    help: 'Duration of timer Increment in ms',
    register: register,
    labelNames: ['pod_name']
});

const summary = new client.Summary({
    name: 'counter_increment_summary_ms',
    help: 'Duration of counter increment summary in ms',
    register: register,
    labelNames: ['counter_summary'],
    // buckets for response time from 0.1ms to 500ms
    buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]
});

/**
 * Using Counter 
 */
counter.labels('jc_test').inc();
counter.labels('jc_test_1').inc();

/**
 * Using Guage set method
 */
gauge.set({ pod_name:'jc-test-pod', method: 'GET', statusCode: '200' }, 100);

/**
 * Using Guage startTimer method
 */
const end = gauge.startTimer({ pod_name:'jc-test-pod', method: "GET" });
setTimeout(()=>{ end({statusCode: '500' }); }, 500);

/**
 * Using Guage inc, dec method
 */
gauge1.labels('jc-test-guage-pod').inc();
gauge1.labels('jc-test-guage-pod').inc(5);
gauge1.labels('jc-test-guage-pod').dec();


/**
 * Using Histogram observe method
 */
_counter++; 
histogram.labels('jc-test-pod').observe(_counter);

setInterval(()=>{ 
    _counter++; 
    histogram.labels('jc-test-pod').observe(_counter);
}, 1000);

/**
 * Using Histogram starttimer method
 */
const end1 = histogram1.startTimer({ pod_name:'jc-test-pod' });
setTimeout(() => {
    end1({ pod_name:'jc-test-pod' });
}, 100);
