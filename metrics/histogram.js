const prometheus = require('prom-client');

class Histogram extends prometheus.Histogram {
    constructor(config) {
        config.registers = [config.register];
        super(config);
        this.register = config.register;
    }
    
}

module.exports = Histogram;
