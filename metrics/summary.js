const prometheus = require('prom-client');

class Summary extends prometheus.Summary {
    constructor(config) {
        config.registers = [config.register];
        super(config);
        this.register = config.register;
    }
}

module.exports = Summary;
