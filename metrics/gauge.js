const prometheus = require('prom-client');

class Gauge extends prometheus.Gauge {
    constructor(config) {
        config.registers = [config.register];
        super(config);
        this.register = config.register;
    }
}

module.exports = Gauge;
