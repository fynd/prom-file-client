
const prometheus = require('prom-client');

class Counter extends prometheus.Counter {
    constructor(config) {
        config.registers = [config.register];
        super(config);
        this.register = config.register;
    }
}

module.exports = Counter
