const client = require('prom-client');
const path = require('path');
const Registry = client.Registry;
const { fork } = require('child_process');

let SIGTERM = false;

process.on('SIGTERM', () => {
  SIGTERM = true;
  if (
    instance &&
    instance.writerProcess &&
    typeof instance.writerProcess.kill == 'function'
  ) {
    instance.writerProcess.kill('SIGTERM');
  }
});

let DEBUG = process.env.DEBUG;

class FileRegistry extends Registry {

    constructor(opts) {
        super();
        opts = Object.assign({ timestamps: false }, opts || {});
        opts.metricsDir = opts.metricsDir || '/tmp/';
        let fileName = process.env.K8S_POD_NAME ? process.env.K8S_POD_NAME : this.genRamdomFilename();
        fileName = `${fileName + '-' + process.pid}.prom`;//adding pid for multi threading support
        opts.metricsFile = this.getFullPath(opts.metricsDir, fileName);
        
        opts.flushIntervalms = opts.flushIntervalms || 4000;
        if(DEBUG == 'true') {
            this.writerProcess = { send : function(message){
                // console.log({message: 'Skipping file writes in debug mode'})
            }}
        } else {
            this.writerProcess = fork(__dirname + '/writerProcess.js');
        }
        this.fileOpts = opts;
        this.startFlusher(opts.flushIntervalms);
    }

    getFullPath(pathName, fileName){
        return path.join(pathName, fileName);
    }

    genRamdomFilename(){
        return 'metrics-' + (Math.random() * 100000000);
    }

    startFlusher(flushIntervalms) {
        this.flusherIntervalId = setInterval(async () => { await this.flushAll(); }, flushIntervalms);
    }

    stopFlusher() {
        this.flusherIntervalId && clearInterval(this.flusherIntervalId);
    }

    async flushAll() {
        const { metricsFile, timestamps } = this.fileOpts;
        let promStr = await this.metrics({ timestamps });
        this.fileWriter(metricsFile, promStr);
    }

    fileWriter(file, data) {
        if(SIGTERM) {
            return ;
        }
        this.writerProcess.send({file: file, data: data });
    }

}

let instance = null;
exports.getInstance = function (options) {
    if (!instance) {
        instance = new FileRegistry(options);
    }
    return instance;
};

