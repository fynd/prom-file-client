let prometheus = require('./prometheus');
let fileRegistry = require('./fileRegistry');

module.exports = {
    prometheus: prometheus,
    fileRegistry: fileRegistry
}
